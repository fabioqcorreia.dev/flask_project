from flask import Flask

APP = Flask(__name__)


@APP.route('/<string:name>', methods=['GET', 'POST'])
def hello_world(name):
    return f'Hello {name}!'


if __name__ == '__main__':
    APP.run()
